class Car
  attr_accessor :car_mode
  def start(turn:)
    raise "Error data valid." unless turn.is_a?(String)
    if turn == "start"
      puts "Please white for starting car."
      sleep(3)
      puts "Is start car"
      @car_mode  = "start"
    end
  end

  def move(x: , y:)
    "moving now toward #{x}.x #{y}.y" if @car_mode == "start"
    "turn_of, please turn on the car."
  end

  def on_lamp
    "On lamp front." if @car_mode == "start" || @car_mode == "moving"
    "turn_of, please turn on the car."
  end

  def on_sound(turn:)
    return "Play sound..." if turn.is_a?(TrueClass) && (@car_mode == "start" || @car_mode == "moving")
  end

  def stop(turn:)
    return "Stoooop!!" if turn.is_a?(TrueClass) && @car_mode == "moving"
  end

  def change_gear(handle:)
    gears  = [1,2,3,4]
    "The car is in #{gears[handle + 1]}" unless handle > 4

  end

end


